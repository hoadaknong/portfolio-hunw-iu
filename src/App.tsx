import { useRef, useState } from "react";
import Container from "./components/Container";
import Header from "./components/Header";
import Intro from "./pages/Intro";
import About from "./pages/About";
import Education from "./pages/Education";
import Skill from "./pages/Skill";
import Project from "./pages/Project";
import Certificate from "./pages/Cetificate";
import Footer from "./components/Footer";
import useTheme from "./hooks/useTheme";
import Button from "./components/Button";
import { Tooltip, Drawer, Radio, RadioChangeEvent } from "antd";

import { AiOutlineSetting, AiOutlineMenu } from "react-icons/ai";
import { chosenColor, colorsTheme, navigationList, setChosenColor } from "./data/Data";
import SocialSideBar from "./components/SocialSideBar";
import Contact from "./pages/Contact";
function App() {
  const { currentMode, currentColor, setCurrenColor } = useTheme();
  const commonClassName: string = "w-full transition-all duration-300";
  const [open, setOpen] = useState<boolean>(false);
  const [openMenu, setOpenMenu] = useState<boolean>(false);
  const introRef = useRef<any>(null);
  const aboutRef = useRef<any>(null);
  const certificateRef = useRef<any>(null);
  const contactRef = useRef<any>(null);
  const educationRef = useRef<any>(null);
  const projectRef = useRef<any>(null);
  const skillRef = useRef<any>(null);
  const showDrawer = () => {
    setOpen(true);
  };

  const onClose = () => {
    setOpen(false);
  };
  const showDrawerMenu = () => {
    setOpenMenu(true);
  };

  const onCloseMenu = () => {
    setOpenMenu(false);
  };
  const onChange = (e: RadioChangeEvent) => {
    setChosenColor(e.target.value as string);
    setCurrenColor(e.target.value as string);
  };
  const menuClick = (orderMenu: number) => {
    switch (orderMenu) {
      case 0:
        aboutRef.current.scrollIntoView();
        break;
      case 1:
        educationRef.current.scrollIntoView();
        break;
      case 2:
        skillRef.current.scrollIntoView();
        break;
      case 3:
        certificateRef.current.scrollIntoView();
        break;
      case 4:
        projectRef.current.scrollIntoView();
        break;
    }
  };
  return (
    <Container
      className={
        currentMode.includes("dark")
          ? `dark bg-dark ` + commonClassName
          : commonClassName + " bg-light"
      }>
      <Drawer
        title="Setting theme"
        placement="right"
        onClose={onClose}
        open={open}
        drawerStyle={{ backgroundColor: "#e3e3e3" }}>
        <Container>
          <Radio.Group value={chosenColor} onChange={onChange} className="flex flex-col gap-8 mt-8">
            {colorsTheme.map((color) => {
              return (
                <Container
                  className="grid grid-cols-2 gap-3 justify-start items-center"
                  key={color}>
                  <Radio value={color} className="text-xl" style={{ color: color }}>
                    {color}
                  </Radio>
                  <Container className="w-5 h-5" style={{ backgroundColor: color }}></Container>
                </Container>
              );
            })}
          </Radio.Group>
        </Container>
      </Drawer>
      <Drawer
        title="Menu"
        placement="left"
        onClose={onCloseMenu}
        open={openMenu}
        drawerStyle={{ backgroundColor: "#e3e3e3" }}>
        <Container className="flex flex-col">
          {navigationList.map((nav, _) => {
            return (
              <Container
                key={_}
                onClick={() => {
                  if (menuClick) menuClick(_);
                  onCloseMenu();
                }}
                className="dark:hover:bg-gray-800 hover:bg-gray-100 flex justify-center items-center transition-all duration-300 cursor-pointer w-full h-[60px]">
                {nav}
              </Container>
            );
          })}
        </Container>
      </Drawer>
      <SocialSideBar />
      <Container className="flex relative dark:bg-main-dark-bg">
        <Container className="fixed right-5 bottom-5" style={{ zIndex: "1000" }}>
          <Tooltip title="Setting">
            <Button
              onClick={showDrawer}
              style={{ background: currentColor, borderRadius: "50%" }}
              className="text-3xl text-white p-3 shadow transition-all hover:scale-[1.1]"
              icon={<AiOutlineSetting />}
            />
          </Tooltip>
        </Container>
        <Container
          className="min-[1317px]:hidden visible transition-all fixed left-5 bottom-5"
          style={{ zIndex: "1000" }}>
          <Tooltip title="Setting">
            <Button
              onClick={showDrawerMenu}
              style={{ background: currentColor, borderRadius: "50%" }}
              className="text-3xl text-white p-3 shadow transition-all hover:scale-[1.1]"
              icon={<AiOutlineMenu />}
            />
          </Tooltip>
        </Container>
      </Container>
      <Container className="w-full backdrop-blur-md fixed top-0 z-50 h-[80px]">
        <Header menuClick={menuClick} />
      </Container>
      <Container className="flex flex-col justify-center items-center gap-[200px]">
        <Intro
          ref={introRef}
          sayHelloOnClick={() => {
            contactRef.current.scrollIntoView();
          }}
        />
        <About ref={aboutRef} />
        <Education ref={educationRef} />
        <Skill ref={skillRef} />
        <Project ref={projectRef} />
        <Certificate ref={certificateRef} />
        <Contact ref={contactRef} />
      </Container>
      <Footer />
    </Container>
  );
}

export default App;
