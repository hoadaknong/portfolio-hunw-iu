import React from "react";
import Container from "./Container";
import Text from "./Text";

type ButtonProps = {
  label?: string;
  onClick?: (event: React.MouseEvent<HTMLElement>) => void;
  className?: string;
  style?: React.CSSProperties;
  icon?: JSX.Element;
  disabled?: boolean;
};

const Button: React.FC<ButtonProps> = (props: ButtonProps) => {
  return (
    <button
      type="button"
      onClick={props.onClick}
      className={props.className}
      style={props.style}
      disabled={props.disabled}>
      <Text>{props.label}</Text>
      <Container>{props.icon}</Container>
    </button>
  );
};

export default Button;
