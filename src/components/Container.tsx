import React from "react";

type ContainerProps = {
  children?: React.ReactNode;
  className?: string;
  style?: React.CSSProperties;
  id?: string;
  onClick?: (event: React.MouseEvent<HTMLElement>) => void;
};

const Container = React.forwardRef<HTMLDivElement, ContainerProps>((props: ContainerProps, ref) => {
  return (
    <div
      style={props.style}
      className={props.className}
      id={props.id}
      onClick={props.onClick}
      ref={ref}>
      {props.children}
    </div>
  );
});

export default Container;
