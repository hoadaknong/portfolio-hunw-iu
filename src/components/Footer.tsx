import React from "react";
import Container from "./Container";
import Text from "./Text";
import useTheme from "../hooks/useTheme";

const Footer: React.FC = () => {
  const { currentColor } = useTheme();
  return (
    <Container className="w-full flex flex-col justify-center items-center h-[300px] mt-[200px] bg-white border-t text-xl gap-10 dark:bg-[#080e1c]">
      <Text className="uppercase text-[40px] font-bold" style={{ color: currentColor }}>
        Hazel Nguyen
      </Text>
      <Text className="md:text-2xl text-md text-black dark:text-white">
        &#169;Hazel Nguyen . All rights reserved
      </Text>
    </Container>
  );
};

export default Footer;
