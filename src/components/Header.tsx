import React from "react";
import Container from "./Container";
import Text from "./Text";
import useTheme from "../hooks/useTheme";
import { Switch } from "antd";
import { navigationList } from "../data/Data";
import { MdOutlineLightMode, MdOutlineDarkMode } from "react-icons/md";

type HeaderType = {
  menuClick?: (order: number) => void;
};

const Header: React.FC<HeaderType> = ({ menuClick }: HeaderType) => {
  const { currentColor, currentMode, setCurrentMode } = useTheme();
  const modeOnChange = (checked: boolean) => {
    setCurrentMode(checked ? "dark" : "light");
    localStorage.setItem("currentMode", checked ? "dark" : "light");
  };
  return (
    <Container
      style={{ color: currentColor, transform: "translate(-50%, 0)" }}
      className="grid min-[1317px]:grid-cols-4 text-[50px] w-4/5 ml-auto mr-auto justify-center h-[80px] fixed top-0 left-[50%] grid-cols-2">
      <Container className="flex justify-start items-center col-span-1 select-none">
        <Text
          onClick={() => {
            window.scrollTo(0, 0);
          }}
          className="font-bold min-[1098px]:text-[50px] md:text-[45px] sm:text-[40px] text-[35px] transition-all hover:scale-[1.02] cursor-pointer">
          .Hazel
        </Text>
      </Container>
      <Container className="min-[1317px]:flex justify-center items-center text-[20px] col-span-2 hidden">
        {navigationList.map((nav, _) => {
          return (
            <Container
              key={nav}
              onClick={() => {
                if (menuClick) menuClick(_);
              }}
              className="h-full dark:hover:bg-gray-800 hover:bg-gray-100 flex justify-center items-center w-[400px] transition-all duration-300 cursor-pointer">
              {nav}
            </Container>
          );
        })}
      </Container>
      <Container className="flex justify-end items-center col-span-1 text-2xl gap-2">
        <MdOutlineLightMode />
        <Switch
          defaultChecked={String(currentMode).includes("dark") ? true : false}
          onChange={modeOnChange}
          className="dark:bg-gray-300 bg-gray-600"
        />
        <MdOutlineDarkMode />
      </Container>
    </Container>
  );
};

export default Header;
