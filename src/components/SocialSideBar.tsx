import { Tooltip } from "antd";
import { socialList } from "../data/Data";
import useTheme from "../hooks/useTheme";
import { useWindowSize } from "../hooks/useWindowSize";
import Container from "./Container";

const SocialSideBar: React.FC = () => {
  const { currentColor } = useTheme();
  const { width } = useWindowSize();

  return (
    <Container
      className={
        width > 1294
          ? "fixed top-[40%] left-12 flex flex-col justify-center items-center gap-7"
          : width > 767
          ? "fixed top-[40%] left-3 flex flex-col justify-center items-center gap-7"
          : "hidden"
      }>
      {socialList.map((social, _) => {
        return (
          <Tooltip
            title={social.link.includes("linkedin") ? "LinkedIn" : social.link}
            placement="right"
            key={_}>
            <span
              style={{ color: currentColor }}
              onClick={() => {
                if (social.link.includes("linkedin")) {
                  window.open(social.link, "_blank");
                }
              }}
              className="text-3xl">
              {social.icon}
            </span>
          </Tooltip>
        );
      })}
    </Container>
  );
};

export default SocialSideBar;
