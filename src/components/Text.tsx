import React from "react";

type TextProps = {
  children?: React.ReactNode;
  style?: React.CSSProperties;
  className?: string;
  onClick?: (event: React.MouseEvent<HTMLElement>) => void;
};

const Text = (props: TextProps) => {
  return (
    <p className={props.className} style={props.style} onClick={props.onClick}>
      {props.children}
    </p>
  );
};

export default Text;
