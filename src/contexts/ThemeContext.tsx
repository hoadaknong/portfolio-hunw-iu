import { createContext, useState } from "react";
import { chosenColor } from "../data/Data";

type Mode = "light" | "dark";
export type ThemeContextType = {
  currentMode: Mode;
  currentColor: string;
  setCurrentMode: (mode: Mode) => void;
  setCurrenColor: (color: string) => void;
};
type ChildrenProps = {
  children: React.ReactNode;
};
export const ThemeContext = createContext<ThemeContextType | null>(null);

const ThemeProvider: React.FC<ChildrenProps> = ({ children }: ChildrenProps) => {
  const mode = localStorage.getItem("currentMode") as Mode;
  const [currentColor, setCurrenColor] = useState<string>(chosenColor);
  const [currentMode, setCurrentMode] = useState<Mode>(mode || "light");

  return (
    <ThemeContext.Provider value={{ currentColor, currentMode, setCurrenColor, setCurrentMode }}>
      {children}
    </ThemeContext.Provider>
  );
};

export default ThemeProvider;
