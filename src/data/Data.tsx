import Container from "../components/Container";
import Text from "../components/Text";
import { MdSchool } from "react-icons/md";
import { AiOutlineLinkedin, AiOutlineMail, AiOutlinePhone } from "react-icons/ai";
import React from "react";
export const avatar: string =
  "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697361628/hunwf_iu/licmx7xojhxpbrzyrpak.jpg";

export const helloIcon: string = "../assets/hello.png";

export const navigationList: string[] = [
  "About",
  "Education",
  "Skills",
  "Certificates",
  "Experiences",
  // "Contact",
];
type Intro = {
  fullName: string;
  position: string;
  detail: string;
};
export const introData: Intro = {
  fullName: "Thanh Hường",
  position: "International communication",
  detail:
    "I am a conscientious and ambitious individual with a tremendous learning spirit. I have always been taking on new opportunities to cultivatemy potential, accumulate hands-on experience and enhance my abilities.",
};
type AboutChild = {
  title: string;
  detail: string;
  icon: JSX.Element;
};
type About = {
  linkCV: string;
  detail: string;
  list: AboutChild[];
};

export const colorsTheme: string[] = ["#F16775", "#B20000"];

export var chosenColor: string = colorsTheme[0];
export const setChosenColor = (color: string) => {
  chosenColor = color;
};
export const aboutData: About = {
  detail:
    "I am a passionate young Vietnamese with a strong enthusiasm for culture, digital technology, and active participation in social activities. I possess excellent leadership skills, communication abilities, and a high level of creativity. I am not afraid to challenge myself with new experiences. I hope to contribute to the development of youth network and promote collaboration between Thailand and Vietnam through this program.",
  linkCV: "",
  list: [
    {
      title: "Experience",
      detail: "2+ years",
      icon: (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth={1.5}
          stroke="currentColor"
          className="w-6 h-6">
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M16.5 18.75h-9m9 0a3 3 0 013 3h-15a3 3 0 013-3m9 0v-3.375c0-.621-.503-1.125-1.125-1.125h-.871M7.5 18.75v-3.375c0-.621.504-1.125 1.125-1.125h.872m5.007 0H9.497m5.007 0a7.454 7.454 0 01-.982-3.172M9.497 14.25a7.454 7.454 0 00.981-3.172M5.25 4.236c-.982.143-1.954.317-2.916.52A6.003 6.003 0 007.73 9.728M5.25 4.236V4.5c0 2.108.966 3.99 2.48 5.228M5.25 4.236V2.721C7.456 2.41 9.71 2.25 12 2.25c2.291 0 4.545.16 6.75.47v1.516M7.73 9.728a6.726 6.726 0 002.748 1.35m8.272-6.842V4.5c0 2.108-.966 3.99-2.48 5.228m2.48-5.492a46.32 46.32 0 012.916.52 6.003 6.003 0 01-5.395 4.972m0 0a6.726 6.726 0 01-2.749 1.35m0 0a6.772 6.772 0 01-3.044 0"
          />
        </svg>
      ),
    },
    {
      title: "Completed",
      detail: "4+ Projects",
      icon: (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth={1.5}
          stroke="currentColor"
          className="w-6 h-6">
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M12 7.5h1.5m-1.5 3h1.5m-7.5 3h7.5m-7.5 3h7.5m3-9h3.375c.621 0 1.125.504 1.125 1.125V18a2.25 2.25 0 01-2.25 2.25M16.5 7.5V18a2.25 2.25 0 002.25 2.25M16.5 7.5V4.875c0-.621-.504-1.125-1.125-1.125H4.125C3.504 3.75 3 4.254 3 4.875V18a2.25 2.25 0 002.25 2.25h13.5M6 7.5h3v3H6v-3z"
          />
        </svg>
      ),
    },
    {
      title: "Education",
      detail: "DAV",
      icon: (
        <svg
          xmlns="http://www.w3.org/2000/svg"
          fill="none"
          viewBox="0 0 24 24"
          strokeWidth={1.5}
          stroke="currentColor"
          className="w-6 h-6">
          <path
            strokeLinecap="round"
            strokeLinejoin="round"
            d="M4.26 10.147a60.436 60.436 0 00-.491 6.347A48.627 48.627 0 0112 20.904a48.627 48.627 0 018.232-4.41 60.46 60.46 0 00-.491-6.347m-15.482 0a50.57 50.57 0 00-2.658-.813A59.905 59.905 0 0112 3.493a59.902 59.902 0 0110.399 5.84c-.896.248-1.783.52-2.658.814m-15.482 0A50.697 50.697 0 0112 13.489a50.702 50.702 0 017.74-3.342M6.75 15a.75.75 0 100-1.5.75.75 0 000 1.5zm0 0v-3.675A55.378 55.378 0 0112 8.443m-7.007 11.55A5.981 5.981 0 006.75 15.75v-1.5"
          />
        </svg>
      ),
    },
  ],
};

type Education = {
  time: string;
  location: JSX.Element | string;
  color: string;
  icon: JSX.Element;
};

export const educationData: Education[] = [
  {
    time: "2019 - 2022",
    location: (
      <Container>
        <Text className="text-xl">Bac Giang High School For The Gifted</Text>
        <Text className="text-md">English Major</Text>
      </Container>
    ),
    color: "#75C9B7",
    icon: <MdSchool />,
  },
  {
    time: "2022 - Now",
    location: (
      <Container>
        <Text className="text-xl">Diplomatic Academy of Vietnam</Text>
        <Text className="text-md">Major: International Communication</Text>
      </Container>
    ),
    color: "#8F8F8F",
    icon: (
      <Container className="p-2 mx-auto my-auto flex justify-center items-center ml-1">
        <img src="https://static.dav.edu.vn/App_themes/images/logo-footer.png" alt="" />
      </Container>
    ),
  },
];

type SkillSoftware = {
  skillName: string;
  level: string;
};
export const skillSoftwareList: SkillSoftware[] = [
  {
    skillName: "Adobe Premiere",
    level: "Intermediate",
  },
  {
    skillName: "Adobe Photoshop",
    level: "Intermediate",
  },
  {
    skillName: "Capcut",
    level: "Intermediate",
  },
  {
    skillName: "Notion",
    level: "Basic",
  },
  {
    skillName: "Trello",
    level: "Basic",
  },
  {
    skillName: "Microsoft Word",
    level: "Advanced",
  },
  {
    skillName: "Microsoft Powerpoint",
    level: "Advanced",
  },
  {
    skillName: "Microsoft Powerpoint",
    level: "Advanced",
  },
  {
    skillName: "Microsoft Excel",
    level: "Advanced",
  },
];

export const specialistSkillList: string[] = [
  "Good command of digital communication, including writing articles for newspapers and social media, photography, broadcasting and editing media publications",
  "Effective and articulate communication",
  "Teamwork and leadership competency",
  "Creativity and enthusiasm",
  "Flexible adaption to international environment",
];

type CertificateType = {
  title: string;
  detail: string;
  image: any;
  data: string;
};

export const certificateList: CertificateType[] = [
  {
    title: "International English Language Testing System - IELTS",
    detail: "Band: 7.5",
    image: (
      <img
        src="https://cdn.worldvectorlogo.com/logos/british-council-1.svg"
        className="rounded-xl"
        width={80}
        alt=""
      />
    ),
    data: "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697988969/cer/vznu6xwfnqim6qnz4h2i.jpg",
  },
  {
    title: "Microsoft Office Specialist 2019 - MOS",
    detail: "Microsoft Word",
    image: (
      <img
        src="https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/Microsoft_Office_Word_%282019%E2%80%93present%29.svg/2203px-Microsoft_Office_Word_%282019%E2%80%93present%29.svg.png"
        width={80}
        alt=""
      />
    ),
    data: "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697988970/cer/lhbdykns5iexxoinbkjz.jpg",
  },
  {
    title: "Microsoft Office Specialist 2019 - MOS",
    detail: "Microsoft Powerpoint",
    image: (
      <img
        src="https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Microsoft_Office_PowerPoint_%282019%E2%80%93present%29.svg/512px-Microsoft_Office_PowerPoint_%282019%E2%80%93present%29.svg.png?20210821050414"
        width={80}
        alt=""
      />
    ),
    data: "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697988970/cer/lhbdykns5iexxoinbkjz.jpg",
  },
  {
    title: "Microsoft Office Specialist 2019 - MOS",
    detail: "Microsoft Excel",
    image: (
      <img
        src="https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Microsoft_Office_Excel_%282019%E2%80%93present%29.svg/826px-Microsoft_Office_Excel_%282019%E2%80%93present%29.svg.png"
        width={80}
        alt=""
      />
    ),
    data: "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697988970/cer/lhbdykns5iexxoinbkjz.jpg",
  },
];

type ProjectType = {
  title: string;
  details: JSX.Element;
  images: string[];
};

export const projectList: ProjectType[] = [
  {
    title: "VTV3 - Entertainment Program Production Team",
    details: (
      <>
        <p>
          <strong>VTV3 - Entertainment Program Production Team &bull; Intern</strong>
        </p>
        <p>
          <em>(June 2023 - Present)</em>
          <br />
        </p>
        <ul>
          <li>
            • Participated in script development and assisted in the production of &quot;Trạng
            nguy&ecirc;n nh&iacute; 2023&quot;.
          </li>
          <li>• Supported filming and post-production of the program.</li>
        </ul>
      </>
    ),
    images: [
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555591/project/syaphuz7n5es8qiowrky.png",
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555591/project/th03ad1ufca6tjxsuwpc.png",
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555590/project/la2hs2adakl5ylzjm8ja.png",
    ],
  },
  {
    title: "The PsyMics Project",
    details: (
      <>
        <p>
          <strong>The PsyMics Project &bull; Co-founder and Coordinator</strong>
          <br />
          <em>(April 2021 - January 2022)</em>
        </p>

        <ul>
          <li>• A psychological project for students and young people</li>
          <li>
            • Organized a talk show and an international seminar in collaboration with the Thai
            Embassy.
          </li>
          <li>
            • Contacted and worked with guests: Hiếu PC, Mr. Amorn Chomchoey (Deputy
            Secretary-General, NCSA Thailand).
          </li>
        </ul>
      </>
    ),
    images: [
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555591/project/syaphuz7n5es8qiowrky.png",
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555591/project/th03ad1ufca6tjxsuwpc.png",
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555590/project/la2hs2adakl5ylzjm8ja.png",
    ],
  },
  {
    title: "iSmartkids Self-defense Project",
    details: (
      <>
        <p>
          <strong>
            iSmartkids Self-defense Project &bull; Founder and Head of Organizing&nbsp;Committee
          </strong>
        </p>
        <p>
          <em>(April 2019 - February 2021)</em>
        </p>
        <ul>
          <li>• Managed communication and handled internal and external affairs.</li>
          <li>• Directed the communication plan and created content.</li>
        </ul>
      </>
    ),
    images: [
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555591/project/syaphuz7n5es8qiowrky.png",
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555591/project/th03ad1ufca6tjxsuwpc.png",
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555590/project/la2hs2adakl5ylzjm8ja.png",
    ],
  },
  {
    title: "BAGI Debate Community",
    details: (
      <>
        <p>
          <strong>BAGI Debate Community &bull; Co-founder and Head of Events</strong>
        </p>
        <p>
          <em>(January 2021 - October 2021)</em>
        </p>
        <ul>
          <li>
            •Deputy Head of the Organizing Committee for the &quot;Lychee Debate Open 2021&quot;
            tournament, fundraising ideas to support Covid-19 in Bac Giang.
          </li>
        </ul>
      </>
    ),
    images: [
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555591/project/syaphuz7n5es8qiowrky.png",
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555591/project/th03ad1ufca6tjxsuwpc.png",
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555590/project/la2hs2adakl5ylzjm8ja.png",
    ],
  },
  {
    title: "Student Union of the Diplomatic Academy",
    details: (
      <>
        <p>
          <strong>Student Union of the Diplomatic Academy &bull; External Affairs</strong>
        </p>
        <p>
          <strong>(November 2022 - Present)</strong>
        </p>
        <ul>
          <li>• External affairs team for the Development Fund and DAV Job Fair 2023</li>
        </ul>
      </>
    ),
    images: [
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555591/project/syaphuz7n5es8qiowrky.png",
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555591/project/th03ad1ufca6tjxsuwpc.png",
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555590/project/la2hs2adakl5ylzjm8ja.png",
    ],
  },
  {
    title: "Faculty of FICC at DAV",
    details: (
      <>
        <p>
          <strong>
            Faculty of FICC at DAV &bull; Collaborator in the &quot;One Day as DAV Communication
            Student&quot; program.
          </strong>
        </p>
      </>
    ),
    images: [
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555591/project/syaphuz7n5es8qiowrky.png",
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555591/project/th03ad1ufca6tjxsuwpc.png",
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555590/project/la2hs2adakl5ylzjm8ja.png",
    ],
  },
  {
    title: "Youth Union of the Ministry of Foreign Affairs",
    details: (
      <>
        <p>
          <strong>
            Youth Union of the Ministry of Foreign Affairs &bull; Youth Union Committee Abroad.
          </strong>
        </p>
      </>
    ),
    images: [
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555591/project/syaphuz7n5es8qiowrky.png",
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555591/project/th03ad1ufca6tjxsuwpc.png",
      "https://res.cloudinary.com/dfnnmsofg/image/upload/v1697555590/project/la2hs2adakl5ylzjm8ja.png",
    ],
  },
];

type SocialLink = {
  icon: any;
  link: string;
};
export const socialList: SocialLink[] = [
  {
    icon: (
      <AiOutlineMail className="hover:scale-[1.1] transition-all duration-200 cursor-pointer" />
    ),
    link: "nguyenthanhhuong.contact@gmail.com",
  },
  {
    icon: (
      <AiOutlinePhone className="hover:scale-[1.1] transition-all duration-200 cursor-pointer" />
    ),
    link: "0968858104",
  },
  {
    icon: (
      <AiOutlineLinkedin className="hover:scale-[1.1] transition-all duration-200 cursor-pointer" />
    ),
    link: "https://www.linkedin.com/in/nguy%E1%BB%85n-thanh-h%C6%B0%E1%BB%9Dng/",
  },
];
type ContactType = {
  icon: React.ReactNode;
  contactType: string;
  detail: string;
};

export const contactTypeList: ContactType[] = [
  { icon: <AiOutlineMail />, contactType: "Email", detail: "nguyenthanhhuong.contact@gmail.com" },
  { icon: <AiOutlinePhone />, contactType: "Phone", detail: "0968 858 104" },
  {
    icon: <AiOutlineLinkedin />,
    contactType: "LinkedIn",
    detail: "www.linkedin.com/in/nguyễn-thanh-hường/",
  },
];
