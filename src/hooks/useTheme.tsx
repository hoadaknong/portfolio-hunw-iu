import { useContext } from "react";
import { ThemeContext } from "../contexts/ThemeContext";

const useTheme: any = () => {
  const context = useContext(ThemeContext);
  if (context) {
    return context;
  }
  throw new Error("Theme error");
};

export default useTheme;
