import React from "react";
import Container from "../components/Container";
import { Image } from "antd";
import { aboutData, avatar } from "../data/Data";
import Text from "../components/Text";
import useTheme from "../hooks/useTheme";
import Button from "../components/Button";
import { BsFileEarmarkPdf } from "react-icons/bs";
type AboutProps = {
  name?: string;
};

const About = React.forwardRef<HTMLDivElement, AboutProps>((props, ref) => {
  const { currentColor } = useTheme();
  return (
    <Container
      className="min-[1341px]:w-4/5 min-[1290px]:w-5/6 w-[90%] grid grid-cols-1 gap-10 pt-[80px]"
      ref={ref}>
      <Container style={{ color: currentColor }}>
        <Text className="font-extrabold text-[40px] text-center">About</Text>
      </Container>
      <Container className="grid md:grid-cols-2 grid-cols-1">
        <Container className="md:border-r border-r-0 border-gray-300">
          <Container className="sm:w-5/6 w-full rounded-[50px] overflow-hidden mx-auto">
            <Image src={avatar} className="rounded-[50px]" />
          </Container>
        </Container>
        <Container className="flex flex-col w-full gap-7 sm:px-10 px-2">
          <Container className="grid sm:grid-cols-3 grid-cols-1 sm:gap-0 gap-3 select-none">
            {aboutData.list.map((e: any, _) => {
              return (
                <Container
                  className={
                    e.title.includes("Completed")
                      ? "flex justify-center items-start transition-all hover:scale-105 duration-300"
                      : e.title.includes("Education")
                      ? "flex justify-end items-start transition-all hover:scale-105 duration-300"
                      : "flex justify-start items-start transition-all hover:scale-105 duration-300"
                  }
                  key={_}>
                  <Container
                    className="py-4 flex flex-col justify-center items-center gap-2 rounded-xl md:w-[70%] sm:w-[90%] w-full"
                    style={{ border: `1px solid ${currentColor}`, color: currentColor }}>
                    <Container>{e.icon}</Container>
                    <Container className="dark:text-white text-black font-semibold text-lg">
                      {e.title}
                    </Container>
                    <Container className="dark:text-gray-200 text-gray-600 text-md">
                      {e.detail}
                    </Container>
                  </Container>
                </Container>
              );
            })}
          </Container>
          <Container>
            <Text className="sm:text-2xl text-lg text-gray-500 dark:text-gray-100 text-justify sm:leading-relaxed leading-relaxed">
              {aboutData.detail}
            </Text>
          </Container>
          <Container>
            <Button
              icon={<BsFileEarmarkPdf className="text-white" />}
              label={"Download CV"}
              onClick={() => {
                window.open(
                  "https://drive.google.com/uc?id=1s0uYrheGg_OjudMyHFK21EbyCbyRLd61&export=download",
                  "_blank",
                );
              }}
              className="flex justify-center items-center gap-3 px-7 py-4 rounded-xl text-white transition-all hover:shadow"
              style={{ backgroundColor: currentColor }}
            />
          </Container>
        </Container>
      </Container>
    </Container>
  );
});

export default About;
