import React from "react";
import Container from "../components/Container";
import Text from "../components/Text";
import useTheme from "../hooks/useTheme";
import { certificateList } from "../data/Data";
type CertificateProps = {
  name?: string;
};
const Cetificate = React.forwardRef<HTMLDivElement, CertificateProps>((props, ref) => {
  const { currentColor } = useTheme();
  return (
    <Container
      className="min-[1341px]:w-4/5 min-[1290px]:w-5/6 w-[90%] grid grid-cols-1 gap-10 pt-[80px]"
      ref={ref}>
      <Container style={{ color: currentColor }}>
        <Text className="font-extrabold text-[40px] text-center">Scholarships & Certificates</Text>
      </Container>
      <Container className="grid grid-cols-1 gap-10 w-full select-none">
        {certificateList.map((cer, _) => {
          return (
            <Container
              key={_}
              className="grid sm:grid-cols-12 grid-cols-1 gap-3 border dark:border-gray-200 border-gray-700 rounded-lg px-3 py-4 dark:bg-gray-500 bg-white sm:justify-start sm:items-center justify-center items-center hover:scale-[1.01] transition-all duration-300 cursor-pointer">
              <Container className="flex col-span-1 justify-center items-center md:text-3xl text-md">
                {cer.image}
              </Container>
              <Container className="sm:col-span-11 col-span-1 flex flex-col gap-2 justify-center items-start sm:text-start text-center">
                <Text className="dark:text-white text-gray-800">{cer.title}</Text>
                <Text className="dark:text-gray-200 text-gray-600">{cer.detail}</Text>
              </Container>
            </Container>
          );
        })}
      </Container>
    </Container>
  );
});

export default Cetificate;
