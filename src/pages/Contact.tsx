import React, { useState } from "react";
import Container from "../components/Container";
import { BsFileEarmarkPdf } from "react-icons/bs";
import Button from "../components/Button";
import Text from "../components/Text";
import useTheme from "../hooks/useTheme";
import { contactTypeList } from "../data/Data";
import emailjs from "@emailjs/browser";
import { Tooltip, message } from "antd";
import { PiCopySimpleLight } from "react-icons/pi";
type ContactProps = {
  name?: string;
};

const Contact = React.forwardRef<HTMLDivElement, ContactProps>((props, ref) => {
  const { currentColor } = useTheme();
  const [mailParam, setMailParam] = useState({ email: "", name: "", detail: "" });
  const [loading, setLoading] = useState(false);
  const validate = ({ email, name, detail }: { email: string; name: string; detail: string }) => {
    if (email.trim() === "") {
      message.error("Enter your email, please!");
      return false;
    }
    if (name.trim() === "") {
      message.error("Enter your name, please!");
      return false;
    }
    if (detail.trim() === "") {
      message.error("Enter your message, please!");
      return false;
    }
    return true;
  };
  const sendMail = (e: any) => {
    setLoading(true);
    if (validate({ ...mailParam })) {
      emailjs.init("oXNKICSxlronvfrHH");
      emailjs.send("service_r02vk2j", "template_0z31bca", mailParam).then(
        function (response) {
          message.success("Email was sent successfully!");
          setMailParam({ email: "", name: "", detail: "" });
        },
        function (err) {
          message.error("Email was sent failed!");
        },
      );
    }
    setLoading(false);
  };
  return (
    <Container
      className="min-[1341px]:w-4/5 min-[1290px]:w-5/6 w-[90%] grid grid-cols-1 gap-10 pt-[80px]"
      ref={ref}>
      <Container style={{ color: currentColor }}>
        <Text className="font-extrabold text-[40px] text-center">Contact</Text>
      </Container>
      <Container className="grid md:grid-cols-2 grid-cols-1 select-none">
        <Container className="md:border-r border-r-0 border-gray-300">
          <Container className="sm:w-5/6 w-full mx-auto flex flex-col gap-10">
            <Text className="dark:text-white text-black text-3xl mx-auto">My networks</Text>
            {contactTypeList.map((contact) => {
              return (
                <Container
                  key={contact.contactType}
                  className="border dark:border-gray-200 border-gray-600 rounded-lg p-4 py-6 flex items-center justify-start gap-3 dark:bg-slate-600 bg-white transition-all duration-200 hover:scale-[1.04] hover:drop-shadow-md overflow-hidden">
                  <Container
                    className="rounded-xl p-3 text-white text-2xl drop-shadow-md"
                    style={{ backgroundColor: currentColor }}>
                    {contact.icon}
                  </Container>
                  <Container>
                    <Container className="flex justify-start items-center gap-2">
                      <Text className="dark:text-white text-black text-lg">
                        {contact.contactType}
                      </Text>
                      <Tooltip title="Copy">
                        <span
                          className="cursor-pointer"
                          onClick={() => {
                            navigator.clipboard.writeText(contact.detail);
                            message.success("Copy successfully!");
                          }}>
                          <PiCopySimpleLight className="dark:text-gray-200 text-gray-600" />
                        </span>
                      </Tooltip>
                    </Container>
                    <Text className="dark:text-gray-300 text-gray-600 truncate">
                      {contact.detail}
                    </Text>
                  </Container>
                </Container>
              );
            })}
          </Container>
        </Container>
        <Container className="flex flex-col w-full gap-7 sm:px-10 px-2">
          <Text className="dark:text-white text-black text-3xl mx-auto md:mt-0 mt-10">
            Connect with me
          </Text>
          <Container className="flex flex-col items-start justify-start gap-4 w-full">
            <label className="flex items-start justify-start flex-col gap-2 w-full">
              <Text className="dark:text-white text-black text-lg">Name</Text>
              <input
                className="px-3 py-3 rounded-lg border dark:border-gray-200 border-gray-600 w-full dark:bg-dark bg-white dark:text-white text-black"
                value={mailParam.name}
                onChange={(e: any) => {
                  setMailParam({ ...mailParam, name: e.target.value });
                }}
                placeholder="Enter your name..."
              />
            </label>
            <label className="flex items-start justify-start flex-col gap-2 w-full">
              <Text className="dark:text-white text-black text-lg">Email</Text>
              <input
                type="email"
                className="px-3 py-3 rounded-lg border dark:border-gray-200 border-gray-600 w-full dark:bg-dark bg-white dark:text-white text-black"
                value={mailParam.email}
                onChange={(e: any) => {
                  setMailParam({ ...mailParam, email: e.target.value });
                }}
                placeholder="Enter your email..."
              />
            </label>
            <label className="flex items-start justify-start flex-col gap-2 w-full">
              <Text className="dark:text-white text-black text-lg">Project</Text>
              <textarea
                className="px-3 py-3 rounded-lg border dark:border-gray-200 border-gray-600 w-full dark:bg-dark bg-white dark:text-white text-black"
                rows={10}
                value={mailParam.detail}
                onChange={(e: any) => {
                  setMailParam({ ...mailParam, detail: e.target.value });
                }}
                placeholder="Enter your message..."
              />
            </label>
            <Button
              icon={<BsFileEarmarkPdf className="text-white" />}
              label={"Send mail"}
              onClick={sendMail}
              className="flex justify-center items-center gap-3 px-7 py-4 rounded-xl text-white transition-all hover:shadow cursor-pointer disabled:bg-gray-300"
              style={{ backgroundColor: loading ? "" : currentColor }}
              disabled={loading}
            />
          </Container>
        </Container>
      </Container>
    </Container>
  );
});

export default Contact;
