import React from "react";
import Container from "../components/Container";
import Text from "../components/Text";
import useTheme from "../hooks/useTheme";
import { VerticalTimeline, VerticalTimelineElement } from "react-vertical-timeline-component";
import "react-vertical-timeline-component/style.min.css";
import { educationData } from "../data/Data";

type EducationProps = {
  name?: string;
};

const Education = React.forwardRef<HTMLDivElement, EducationProps>((props, ref) => {
  const { currentColor } = useTheme();
  return (
    <Container
      className="min-[1341px]:w-4/5 min-[1290px]:w-5/6 w-[90%] grid grid-cols-1 gap-10 pt-[80px]"
      ref={ref}>
      <Container style={{ color: currentColor }}>
        <Text className="font-extrabold text-[40px] text-center">Education</Text>
      </Container>
      <Container className="grid grid-cols-1 justify-center items-center mt-10">
        <VerticalTimeline lineColor={currentColor}>
          {educationData.map((edu, _) => {
            return (
              <VerticalTimelineElement
                key={_}
                className="vertical-timeline-element--work"
                contentStyle={{ background: edu.color, color: "#fff" }}
                contentArrowStyle={{ borderRight: `7px solid  ${edu.color}` }}
                date={edu.time}
                dateClassName="dark:text-white text-black"
                iconStyle={{ background: edu.color, color: "#fff" }}
                icon={edu.icon}>
                {edu.location}
              </VerticalTimelineElement>
            );
          })}
        </VerticalTimeline>
      </Container>
    </Container>
  );
});

export default Education;
