import React from "react";
import Container from "../components/Container";
import Text from "../components/Text";
import { avatar, introData } from "../data/Data";
import { Image } from "antd";
import useTheme from "../hooks/useTheme";
import Button from "../components/Button";

type IntroProps = {
  name?: string;
  sayHelloOnClick: any;
};

const Intro = React.forwardRef<HTMLDivElement, IntroProps>((props, ref) => {
  const { currentColor } = useTheme();
  return (
    <Container
      className="mt-[200px] min-[1341px]:w-4/5 min-[1290px]:w-5/6 w-[90%] md:grid-cols-2 grid grid-cols-1 transition-all md:gap-0 gap-10"
      ref={ref}
    >
      <Container className="flex justify-start flex-col gap-6 md:row-start-none row-start-2">
        <Container className="relative flex ahihi">
          <Text
            className="font-pacifico min-[1098px]:text-[80px] md:text-[54px] sm:text-[60px] text-[40px] transition-all"
            style={{ color: currentColor }}
          >
            {introData.fullName}
          </Text>
          <Container className="bg-hello w-10 h-10 bg-cover"></Container>
        </Container>
        <Container className="flex justify-start items-center gap-4">
          <Container
            className="max-w-[200px] min-[1341px]:w-1/2 min-[1290px]:w-1/3 min-[1000px]:w-1/4 min-[936px]:w-1/5 w-0 h-[1px] transition-all"
            style={{ backgroundColor: currentColor }}
          ></Container>
          <Container>
            <Text className="sm:text-[25px] text-[20px] text-gray-600 dark:text-gray-100">
              {introData.position}
            </Text>
          </Container>
        </Container>
        <Container>
          <Text className="sm:text-2xl text-lg text-gray-500 dark:text-gray-100 text-justify">
            {introData.detail}
          </Text>
        </Container>
        <Container className="md:mt-10 mt-1">
          <Button
            icon={
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="#fff"
                className="w-6 h-6"
              >
                <path
                  strokeLinecap="round"
                  strokeLinejoin="round"
                  d="M6 12L3.269 3.126A59.768 59.768 0 0121.485 12 59.77 59.77 0 013.27 20.876L5.999 12zm0 0h7.5"
                />
              </svg>
            }
            label={"Say Hello"}
            onClick={props.sayHelloOnClick}
            className="flex justify-center items-center gap-3 px-7 py-4 rounded-xl text-white transition-all hover:shadow"
            style={{ backgroundColor: currentColor, color: "white" }}
          />
        </Container>
      </Container>
      <Container className="flex md:justify-end justify-center items-center md:row-start-2 row-start-1">
        <Container
          className="overflow-hidden min-[1341px]:w-[400px] min-[1290px]:w-[350px] min-[1000px]:w-[300px] w-[350px] s min-[1341px]:h-[400px] min-[1290px]:h-[350px] min-[1000px]:h-[300px] h-[350px] transition-all bg-black shadow-md"
          style={{ borderRadius: "39% 61% 64% 36% / 30% 37% 63% 70%" }}
        >
          <Image src={avatar} />
        </Container>
      </Container>
    </Container>
  );
});

export default Intro;
