import React from "react";
import Container from "../components/Container";
import Text from "../components/Text";
import useTheme from "../hooks/useTheme";
import { projectList } from "../data/Data";
import { Image } from "antd";

type ProjectProps = {
  name?: string;
};
const Project = React.forwardRef<HTMLDivElement, ProjectProps>((props, ref) => {
  const { currentColor, currentMode } = useTheme();
  const iconWhite =
    "https://res.cloudinary.com/dfnnmsofg/image/upload/v1699199805/icon/kafwv9hwrk9xjhlzz3ik.png";
  const iconBlack =
    "https://res.cloudinary.com/dfnnmsofg/image/upload/v1699199806/icon/dm7gsybt5muu6ooqwg6y.png";
  return (
    <Container
      className="min-[1341px]:w-4/5 min-[1290px]:w-5/6 w-[90%] grid grid-cols-1 gap-10 pt-[80px]"
      ref={ref}>
      <Container style={{ color: currentColor }}>
        <Text className="font-extrabold text-[40px] text-center">Experience</Text>
      </Container>
      <Container className="grid grid-cols-1 gap-10 mx-auto divide-y-2 dark:divide-gray-100 divide-gray-600">
        {projectList.map((project, _) => {
          return (
            <Container
              className="overflow-hidden dark:text-white text-black flex flex-col justify-start items-start pt-4 gap-4"
              key={_}>
              <Container className="list-disc list-inside">{project.details}</Container>
              <Container className="hover:underline cursor-pointer dark:text-gray-400 text-gray-500 flex justify-start items-center gap-2">
                <Image.PreviewGroup items={project.images}>
                  <Image width={40} src={currentMode === "light" ? iconBlack : iconWhite} />
                </Image.PreviewGroup>
              </Container>
            </Container>
          );
        })}
      </Container>
    </Container>
  );
});

export default Project;
