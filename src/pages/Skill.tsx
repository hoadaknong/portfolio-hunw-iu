import React from "react";
import Container from "../components/Container";
import Text from "../components/Text";
import { skillSoftwareList, specialistSkillList } from "../data/Data";
import { PiMedalFill } from "react-icons/pi";
import useTheme from "../hooks/useTheme";
type SkillProps = {
  name?: string;
};

const Skill = React.forwardRef<HTMLDivElement, SkillProps>((props, ref) => {
  const { currentColor } = useTheme();
  return (
    <Container
      className="min-[1341px]:w-4/5 min-[1290px]:w-5/6 w-[90%] grid grid-cols-1 gap-10 pt-[80px]"
      ref={ref}>
      <Container style={{ color: currentColor }}>
        <Text className="font-extrabold text-[40px] text-center">Skills</Text>
      </Container>
      <Container className="grid md:grid-cols-2 grid-cols-1 gap-10 w-full">
        <Container className="flex flex-col justify-center items-center gap-4 border dark:border-gray-100 border-gray-600 rounded-3xl pt-7 px-[60px] pb-12">
          <Container className="">
            <Text className="dark:text-white text-gray-800 text-center md:text-3xl text-xl">
              Software
            </Text>
          </Container>
          <Container className="grid md:grid-cols-2 grid-cols-1 gap-4 mt-4 w-full">
            {skillSoftwareList.map((skill, _) => {
              return (
                <Container className="grid grid-cols-4 gap-3" key={_}>
                  <Container className="flex col-span-1 justify-center items-center md:text-3xl text-md">
                    <PiMedalFill className="dark:text-gray-200 text-gray-800" />
                  </Container>
                  <Container className="col-span-3 flex flex-col gap-2">
                    <Text className="dark:text-white text-gray-800">{skill.skillName}</Text>
                    <Text className="dark:text-gray-400 text-gray-400">{skill.level}</Text>
                  </Container>
                </Container>
              );
            })}
          </Container>
        </Container>
        <Container className="flex flex-col justify-start items-center gap-4 border dark:border-gray-100 border-gray-600 rounded-3xl pt-7 px-12 pb-12">
          <Container className="">
            <Text className="dark:text-white text-gray-800 text-center md:text-3xl text-xl">
              Specialist Skills
            </Text>
          </Container>
          <Container className="grid grid-cols-1 gap-7 mt-4">
            {specialistSkillList.map((skill, _) => {
              return (
                <Container className="grid grid-cols-6 gap-3" key={_}>
                  <Container className="flex col-span-1 justify-center items-center md:text-3xl text-md">
                    <PiMedalFill className="dark:text-gray-200 text-gray-800" />
                  </Container>
                  <Container className="col-span-5 flex flex-col gap-2">
                    <Text className="dark:text-white text-gray-800">{skill}</Text>
                  </Container>
                </Container>
              );
            })}
          </Container>
        </Container>
      </Container>
    </Container>
  );
});

export default Skill;
