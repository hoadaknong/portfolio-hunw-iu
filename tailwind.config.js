/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: "class",
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    fontFamily: {
      "tilt-neon": ["Tilt Neon", "sans-serif"],
      pacifico: ["Pacifico", "serif"],
    },
    extend: {
      backgroundColor: {
        dark: "#0E172B",
        light: "#F6F6F6",
      },
      backgroundImage: {
        hello: "url('./assets/hello.png')",
      },
    },
  },
  plugins: [],
};
